package com.institute.controller;


import com.institute.domain.Institute;
import com.institute.domain.InstituteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/institute")
public class InstituteController {
	
	@Autowired
	private InstituteRepository repository;
	
	@GetMapping
	public Flux<Institute> findInstitutes() {
		log.info("Mostrando todos los datos.");
		return repository.findAll();
	}
	
	@GetMapping("/{id}")
	public Mono<Institute> findInstituteById(@PathVariable Long id) {
		log.info("Mostrando dato con el id: "+ id);
		return repository.findById(id);
	}
	
	@PostMapping("/create")
	public Mono<Institute> newInstitute(@RequestBody Institute data) {
		if (data == null) {
			log.error("Problemas al recoger datos");
			return null;
		}
		log.info("Nuevo instituto registrado: "+ data.toString());
		return repository.save(data);
	}
	
	@PutMapping("/update")
	public Mono<Institute> modInstitute(@RequestBody Institute data) {
		if (data.getId_inst() == null) {
			log.error("Problemas al recoger datos");
			return null;
		}
		log.info("Nuevo instituto actualizado: "+ data.toString());
		return repository
				.findById(data.getId_inst())
				.map(
					(c) -> {
						c.setId_inst(data.getId_inst());
						c.setRuc_inst(data.getRuc_inst());
						c.setNom_inst(data.getNom_inst());
						c.setCel_inst(data.getCel_inst());
						c.setEma_inst(data.getEma_inst());
						c.setDir_inst(data.getDir_inst());
						c.setTip_inst(data.getTip_inst());
						c.setEst_inst(data.getEst_inst());
						return c;
					}
				)
				.flatMap(
					c -> repository.save(c)
				);
	}
	
	@DeleteMapping("/delete/{id}")
	public Mono<Void> delInstitute(@PathVariable Long id) {
		if (id == 0 || id == null) {
			log.error("Problemas al recoger datos");
			return null;
		}
		log.info("Nuevo instituto eliminado: "+ id);
		return repository.deleteById(id);
	}
}