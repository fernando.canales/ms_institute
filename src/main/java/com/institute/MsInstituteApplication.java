package com.institute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsInstituteApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsInstituteApplication.class, args);
	}
}