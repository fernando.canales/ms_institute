package com.institute.domain;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InstituteRepository extends ReactiveCrudRepository<Institute, Long> {
}