package com.institute.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "institute")
public class Institute {

	@Id private Long id_inst;
	@Column private String ruc_inst;
	@Column private String nom_inst;
	@Column private String cel_inst;
	@Column private String ema_inst;
	@Column private String dir_inst;
	@Column private String tip_inst;
	@Column private String est_inst;
}